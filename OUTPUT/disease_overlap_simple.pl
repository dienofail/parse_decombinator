#Disease_Overlap_Simple.pl is a script to find only the CDR3 overlap between my AA_UNIQUE_OUTPUT files.

use warnings;
use strict;
use Cwd;
use Text::Match::FastAlternatives;
use List::Compare;
my $dir = cwd;


my $inputfile = 'Combined_GBSET_paper_cdr3.txt';

my @cdr3s;
my %disease;
my %diseaseuniquecount;
my %diseasetotalcount;
my $permanentoutput = 'Disease_Simple_Summary.txt';
open(INPUT, ">$permanentoutput") or die;
close(INPUT);



open(INPUT, $inputfile) or die;

while (my $line = <INPUT>) {
    $line =~ s/\n$//;
    my @currentline = split(/\s+/,$line);
    if ($#currentline > 1) {
        $disease{$currentline[1]} = \@currentline;
        #$disease{$realkey}            = \@currentline;
		$diseaseuniquecount{$currentline[1]} = 0;
		$diseasetotalcount{$currentline[1]}  = 0;
    }
}
close(INPUT);

my @filelist;
my $file;

opendir( DIR, $dir ) or die "can't opendir $dir: $!";
while ( defined( $file = readdir(DIR) ) ) {
	next if $file =~ /^\.\.?$/;                # skip . and ..
	next if $file =~ m/Decombinator_output/;
	next if $file =~ /tags/;
	next if $file =~ /Summary/;
	next if $file =~ /summary/;
	next if $file =~ /Combined/;
	next if $file =~ /analyze/;
    next if $file =~ /GBSET/;
	#next if $file =~ /CD8/;
	if ( $file =~ m/AA_UNIQUE_OUTPUT.txt/ ) {
		push( @filelist, $file );              #read in all files from directory
		print("$file\n");
	}
}
closedir(DIR);

for my $i (0..$#filelist)
{
    main($filelist[$i]);
}

sub main
{
    my %currenthash;
    my $inputfile1 = shift;

    open(INPUT, "$inputfile1");
    print ("Currently on $inputfile1\n");
    while (my $line = <INPUT>) {
        $line =~ s/\n$//;
        my @currentline = split(/\t/,$line);
        $currenthash{$currentline[1]} = $currentline[3];
    }
    close(INPUT);
    my @inputarray = keys %currenthash;
    my @diseasecdr3s = keys %disease;
    my $intersection = special_compare(\@diseasecdr3s, \@inputarray);
    my @intersection = @{$intersection};
    for my $i ( 0 .. $#intersection ) {
            print "My intersect is: $intersection[$i]->[0]\n$intersection[$i]->[1]\n";
    }
    my $outputfile = $inputfile1;
    $outputfile =~ s/AA_UNIQUE_OUTPUT/Disease_Simple_Overlap/;
    $outputfile = "$dir/Disease_Simple/$outputfile";
    open( OUTPUT, ">$outputfile" );
    my $intersectcount      = 0;
    my $intersecttotalcount = 0;
    for my $i ( 0 .. $#intersection ) {
            $intersectcount++;
            print "$currenthash{$intersection[$i]->[1]}\n";
            print OUTPUT "@{$disease{$intersection[$i]->[0]}}\t";
            print OUTPUT "$currenthash{$intersection[$i]->[1]}\n";
            $diseaseuniquecount{ $intersection[$i]->[0] }++;
            $diseasetotalcount{ $intersection[$i]->[0] } +=
              $currenthash{ $intersection[$i]->[1] };
            $intersecttotalcount += $currenthash{ $intersection[$i]->[1] };
    }
    close(OUTPUT);
    open( OUTPUT, ">>$permanentoutput" );
    {
            print OUTPUT "$inputfile1\t$intersectcount\t$intersecttotalcount\n";
    }
    close(OUTPUT);
}


sub special_compare {
	my $arrayone = shift;
	my $arraytwo = shift;
	my @arrayone = @{$arrayone};
	my @arraytwo = @{$arraytwo};
	my @answerarray;
	my @arrayofindecies;
	for my $i ( 0 .. $#arrayone ) {
		for my $j ( 0 .. $#arraytwo ) {
			if (   $arrayone[$i] =~ /$arraytwo[$j]/)
			{
				my @topush;
				push(@topush, $i);
				push(@topush, $j);
				push(@arrayofindecies, \@topush)
			}
			elsif ($arraytwo[$j] =~ /$arrayone[$i]/)
			{
				my @topush;
				push(@topush, $i);
				push(@topush, $j);
				push(@arrayofindecies, \@topush)
			}
		}
	}
	for my $i ( 0 .. $#arrayofindecies ) {
		my @currentindex = @{$arrayofindecies[$i]};
		my $currentindex1 = $currentindex[0];
		my $currentindex2 = $currentindex[1];
		my @topusharray = ($arrayone[$currentindex1], $arraytwo[$currentindex2]);
		print("To push array is @topusharray\n");
		push( @answerarray, \@topusharray);
	}
	return \@answerarray;
}
