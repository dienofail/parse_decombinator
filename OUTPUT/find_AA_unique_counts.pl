use strict;
use warnings;
use Cwd;
my $dir = getcwd();
print("$dir\n");
my $permanent_output = "$dir/Counts/Counts_Summary.txt";
open (OUTPUT, ">$permanent_output");
print OUTPUT "File_Name\t" . '1' . "\t" . '1count' . "\t" . '2<x<10' . "\t" . '2<x<10count' . "\t" . '10<x<100' . "\t" . '10<x<100count' . "\t" . '100<x<1000' ."\t" . '100<x<1000count' ."\t" . '1000<x<2000' . "\t" . '1000<x<2000count' . "\t" . '2000<x<3000'. "\t" . '2000<x<3000count' ."\t" . '3000<x' ."\t" . '3000<xcount' . "\tUnique_Count\tTotal_Count\n";
close(OUTPUT);
opendir( DIR, $dir ) or die "can't opendir $dir: $!";
my $file;
my @filelist;
while ( defined( $file = readdir(DIR) ) ) {
	next if $file =~ /^\.\.?$/;                # skip . and ..
	next if $file =~ m/Decombinator_output/;
	next if $file =~ /tags/;
	if ( $file =~ m/AA_UNIQUE_OUTPUT.txt/ ) {
		push( @filelist, $file );              #read in all files from directory
		print("$file\n");
	}
}
closedir(DIR);

for my $i ( 0 .. $#filelist ) {
	main( $filelist[$i] );
}

sub main {
	my %uniquehash;
	my %counthash;
	my $inputfile = shift;
	print("Currently on $inputfile\n");
	my $uniquefile = $inputfile;
	$uniquefile =~ s/AA_UNIQUE_OUTPUT.txt/AA_COUNT.txt/;
	print("My unique file name is $uniquefile\n");
	my $one;
	my $onecount;
	my $twoten;
	my $twotencount;
#	my $oneten;
#	my $onetencount;
	my $tenhundred;
	my $tenhundredcount;
	my $hundredthousand;
	my $hundredthousandcount;
	my $overthousand;
	my $overthousandcount;
	my $overtwothousand;
	my $overtwothousandcount;
	my $overthreethousand;
	my $overthreethousandcount;
	open( INPUT, "$inputfile" );
	my $keycount;
	my $keycount2;
	my $totalcount;
	my $totalcount2;
	while ( my $line = <INPUT> ) {
		$line =~ s/\n$//;
		my @currentline = split( /\t/, $line );
		if ( $#currentline > 2 ) {
			$keycount2++;
			#print ("$currentline[0], $currentline[7], $currentline[1]\n");
			my $realkey = $currentline[3];
			#print("$realkey\n");
			$uniquehash{ $realkey }++;
			$counthash{ $realkey } += $currentline[3];
			$totalcount2 += $currentline[3];
		}
	}
	close(INPUT);
	for my $key (keys %uniquehash)
	{
		$keycount += $uniquehash{$key};
		$totalcount += $counthash{$key};
	}
	print "My keycounts are $keycount and $keycount2\n";
	print "My totalcounts are $totalcount and $totalcount2\n";
	my $outputfile_name = "$dir/Counts/$uniquefile";
	print ("My outputfilename is $outputfile_name\n");
	open (OUTPUT,">$outputfile_name");
	print OUTPUT "Count\tNumber_Unique\tTotal_Count_Unique\tPercent_Number_Unique\tPercent_Total_Count\n";
	for my $key (sort {$a <=> $b} keys %uniquehash)
	{
		my $toprint = 100 * $uniquehash{$key}  / $keycount;
		my $toprinttwo = 100 * $counthash{ $key } / $totalcount;
		print OUTPUT "$key\t$uniquehash{$key}\t$counthash{$key}\t$toprint\t$toprinttwo\n";
		if ( $key < 2 )
		{
			$one += $uniquehash{$key};
			$onecount += $counthash{$key};
			next;
		}
		
		elsif ($key >= 2 && 10 >= $key)
		{
			$twoten += $uniquehash{$key};
			$twotencount += $counthash{$key};
			next;
		}
		
		elsif (100 >= $key && $key > 10)
		{
			$tenhundred  += $uniquehash{$key};
			$tenhundredcount += $counthash{$key};
			next;
		}
		
		elsif (1000 >= $key && $key > 100)
		{
			$hundredthousand  += $uniquehash{$key};
			$hundredthousandcount += $counthash{$key};
			next;
		}
		elsif (2000>=$key && $key > 1000)
		{
			$overthousand  += $uniquehash{$key};
			$overthousandcount += $counthash{$key};
			next;
		}
		elsif (3000>=$key && $key > 2000)
		{
			$overtwothousand  += $uniquehash{$key};
			$overtwothousandcount += $counthash{$key};
			next;
		}
		elsif ($key > 3000)
		{
			$overthreethousand  += $uniquehash{$key};
			$overthreethousandcount += $counthash{$key};
			next;
		}
	}
	close (OUTPUT);
	
	open (OUTPUT, ">>$permanent_output");
	#print OUTPUT "File_Name\t" . '<10' . "\t" . '<10count' . "\t" . '10<x<100' . "\t" . '10<x<100count' . "\t" . '100<x<1000' ."\t" . '100<x<1000count' ."\t" . '1000<x' . "\t" . '1000<xcount' . "\n";
	print OUTPUT "$inputfile\t$one\t$onecount\t$twoten\t$twotencount\t$tenhundred\t$tenhundredcount\t$hundredthousand\t$hundredthousandcount\t$overthousand\t$overthousandcount\t$overtwothousand\t$overtwothousandcount\t$overthreethousand\t$overthreethousandcount\t$keycount\t$totalcount\n";
	close(OUTPUT);
}
