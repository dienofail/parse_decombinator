use warnings;
use strict;

#my $inputone = 'CD4_combined_output.txt';
my $inputtwo = 'CD4_CD8_total_output.txt';
my $inputthree = 'Guobing_8_overlapping_CDR3s.txt';

my @Guobingarray;
my %hits;
open (INPUT, "$inputthree");
while (my $line = <INPUT>)
{
	$line =~ s/\n$//;
	my @currentline = split (/\t/,$line);
	if ($#currentline == 1)
	{
		my $topush = join("\t", $currentline[0], $currentline[1]);
		print "Currently pushing $topush";
		push(@Guobingarray, $topush);
		$hits{$topush} = [];
	}
}
close(INPUT);

open (INPUT, "$inputtwo");
while (my $line = <INPUT>)
{
	$line =~ s/\n$//;
	my @currentline = split (/\t/,$line);
	print "Checking $currentline[0] and $currentline[1]\n";
	if ($#currentline > 1)
	{
		my $topush = join("\t", $currentline[0], $currentline[1]);
		for my $i (0..$#Guobingarray)
		{
			my @checkarray = split("\t", $Guobingarray[$i]);\
			print "Checking $currentline[0] and $currentline[1] against $checkarray[0] and $checkarray[1]\n";
			if ($checkarray[1] =~ /$currentline[1]/ && $checkarray[0] == $currentline[0])
			{
				push(@{$hits{$Guobingarray[$i]}}, $inputtwo);
			}
		}
	}
}
close(INPUT);

for my $key (keys %hits)
{
	print "$key\t@{$hits{$key}}\n";
}