use warnings;
use strict;
use List::Compare;
use Cwd;

my $dir = cwd;
my $file;
my @filelist;
opendir( DIR, $dir ) or die "can't opendir $dir: $!";
while ( defined( $file = readdir(DIR) ) ) {
	next if $file =~ /^\.\.?$/;                # skip . and ..
	next if $file =~ m/Decombinator_output/;
	next if $file =~ /tags/;
	next if $file =~ /Summary/;
	next if $file =~ /summary/;
	next if $file =~ /Combined/;
	next if $file =~ /analyze/;
	#next if $file =~ /CD8/;
	if ( $file =~ m/AA_UNIQUE_OUTPUT.txt/ ) {
		push( @filelist, $file );              #read in all files from directory
		                                       #print("$file\n");
	}
}
closedir(DIR);

my $overallinputfile = 'Combined_GBSET_paper_cdr3.txt.txt';
my $permanentoutput  = 'Disease_overlap_summary.txt';
open( INPUT, ">$permanentoutput" );
close(INPUT);
my %disease;
my %diseaseuniquecount;
my %diseasetotalcount;
open( INPUT, "$overallinputfile" );
while ( my $line = <INPUT> ) {
	$line =~ s/\n$//;
	my @currentline = split( /\s+/, $line );
	for my $i ( 0 .. $#currentline ) {
		chomp( $currentline[$i] );
	}
	next if ( $currentline[0] eq 'NaN' );
	if ( $#currentline > 1 ) {
		my $realkey =
		  join( "\t", $currentline[0], $currentline[1], $currentline[2] );
		#print("Currently adding $realkey to disease hash\n");
		$disease{$realkey}            = \@currentline;
		$diseaseuniquecount{$realkey} = 0;
		$diseasetotalcount{$realkey}  = 0;
	}
}
my @diseasekeys = keys %disease;
close(INPUT);
for my $i ( 0 .. $#filelist ) {
	main( $filelist[$i] );
}

sub main {
	my $inputfile = shift;
	my %currenthash;
	print "Currently on $inputfile\n";
	open( INPUT, "$inputfile" );
	while ( my $line = <INPUT> ) {
		$line =~ s/\n$//;
		my @currentline = split( /\s+/, $line );
		for my $i ( 0 .. $#currentline ) {
			chomp( $currentline[$i] );
		}
		if ( $#currentline > 1 ) {
			my $realkey =
			  join( "\t", $currentline[0], $currentline[1], $currentline[2] );
			$currenthash{$realkey} = $currentline[3];
			#print "Adding $realkey to currenthash\n";
			#print "Adding this 3rd element $currenthash{$realkey}\n";
			#print "Generating $realkey from $inputfile\n";
		}
	}
	close(INPUT);
	my @current_keys = keys %currenthash;

	#	my $lc = List::Compare->new(\@diseasekeys,\@current_keys);
	#	my @intersection = $lc->get_intersection;
	my $intersection = special_compare( \@diseasekeys, \@current_keys );
	my @intersection = @{$intersection};
	for my $i ( 0 .. $#intersection ) {
		print "My intersect is: $intersection[$i]->[0]\n$intersection[$i]->[1]\n";
	}
	my $outputfile = $inputfile;
	$outputfile =~ s/AA_UNIQUE_OUTPUT/Disease_Overlap/;
	$outputfile = "$dir/Disease/$outputfile";
	open( OUTPUT, ">$outputfile" );
	my $intersectcount      = 0;
	my $intersecttotalcount = 0;
	for my $i ( 0 .. $#intersection ) {
		$intersectcount++;
		print "$currenthash{$intersection[$i]->[1]}\n";
		print OUTPUT "@{$disease{$intersection[$i]->[0]}}\t";
		print OUTPUT "$currenthash{$intersection[$i]->[1]}\n";
		$diseaseuniquecount{ $intersection[$i]->[0] }++;
		$diseasetotalcount{ $intersection[$i]->[0] } +=
		  $currenthash{ $intersection[$i]->[1] };
		$intersecttotalcount += $currenthash{ $intersection[$i]->[1] };
	}
	close(OUTPUT);

	open( OUTPUT, ">>$permanentoutput" );
	{
		print OUTPUT "$inputfile\t$intersectcount\t$intersecttotalcount\n";
	}
	close(OUTPUT);
}

my $diseasetotal = 'Total_disease_ranked.txt';
open( OUTPUT, ">$diseasetotal" );
for my $key ( keys %disease ) {
	print OUTPUT
	  "@{$disease{$key}}\t$diseaseuniquecount{$key}\t$diseasetotalcount{$key}\n";
}
close(OUTPUT);

#subrountine special_compare takes two arrays as inputs and returns an array of elements that overlap between the two
#with the special condition that the middle part of the key can match in any manner
sub special_compare {
	my $arrayone = shift;
	my $arraytwo = shift;
	my @arrayone = @{$arrayone};
	my @arraytwo = @{$arraytwo};
	my @answerarray;
	my @arrayofindecies;

	#decompose;
	my @copyofarrayone;
	my @copyofarraytwo;
	for my $i ( 0 .. $#arrayone ) {
		my $currentelement = $arrayone[$i];
		my @currentelementarray = split( /\t/, $currentelement );
		push( @copyofarrayone, \@currentelementarray );

		#print "@currentelementarray\n";
	}

	for my $i ( 0 .. $#arraytwo ) {
		my $currentelement = $arraytwo[$i];
		my @currentelementarray = split( /\t/, $currentelement );
		push( @copyofarraytwo, \@currentelementarray );

		#print "@currentelementarray\n";
	}
	for my $i ( 0 .. $#copyofarrayone ) {
		for my $j ( 0 .. $#copyofarraytwo ) {
			if (   $copyofarrayone[$i]->[1] =~ /$copyofarraytwo[$j]->[1]/
				&& $copyofarrayone[$i]->[0] == $copyofarraytwo[$j]->[0]
				&& $copyofarrayone[$i]->[2] == $copyofarraytwo[$j]->[2] )
			{
				my @topush;
				push(@topush, $i);
				push(@topush, $j);
				push(@arrayofindecies, \@topush)
			}
			elsif ($copyofarraytwo[$j]->[1] =~ /$copyofarrayone[$i]->[1]/
				&& $copyofarrayone[$i]->[0] == $copyofarraytwo[$j]->[0]
				&& $copyofarrayone[$i]->[2] == $copyofarraytwo[$j]->[2] )
			{
				my @topush;
				push(@topush, $i);
				push(@topush, $j);
				push(@arrayofindecies, \@topush)
			}
		}
	}

	for my $i ( 0 .. $#arrayofindecies ) {
		my @currentindex = @{$arrayofindecies[$i]};
		my $currentindex1 = $currentindex[0];
		my $currentindex2 = $currentindex[1];
		my @topusharray = ($arrayone[$currentindex1], $arraytwo[$currentindex2]);
		print("To push array is @topusharray\n");
		push( @answerarray, \@topusharray);
	}

	return \@answerarray;
}
