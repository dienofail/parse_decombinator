use warnings;
use strict;
use Cwd;

my $dir = cwd;
my $outputdir = "$dir/Fish";
my @Mfilelist = qw(M1_4_3e8CAA_UNIQUE_OUTPUT.txt M1_4_8CAA_UNIQUE_OUTPUT.txt M1_CCAA_UNIQUE_OUTPUT.txt M1CAA_UNIQUE_OUTPUT.txt);
my @Yfilelist = qw(Y1_4_3e8CAA_UNIQUE_OUTPUT.txt Y1_4_8CAA_UNIQUE_OUTPUT.txt Y1_CCAA_UNIQUE_OUTPUT.txt Y1_4CAA_UNIQUE_OUTPUT.txt);
my @Ofilelist = qw(O1_4_3e8CAA_UNIQUE_OUTPUT.txt O1_4_8CAA_UNIQUE_OUTPUT.txt O1_CCAA_UNIQUE_OUTPUT.txt O1CAA_UNIQUE_OUTPUT.txt);

for my $i (0..$#Mfilelist)
{
	main($Mfilelist[$i]);
}

for my $i (0..$#Yfilelist)
{
	main($Yfilelist[$i]);
}


for my $i (0..$#Ofilelist)
{
	main($Ofilelist[$i]);
}



sub main
{
	my $inputfile = shift;
	my $outputfile = $inputfile;
	$outputfile =~ s/AA_UNIQUE_OUTPUT/Fish_Input/;
	$outputfile = "$outputdir/$outputfile";
	open(INPUT, "$inputfile");
	open (OUTPUT, ">$outputfile");
	while (my $line = <INPUT>)
	{
		$line =~ s/\n$//;
		my @currentline = split(/\s+/, $line);
		my $realkey = join( ',', $currentline[0], $currentline[1], $currentline[2] );
		print OUTPUT "$realkey\t$currentline[3]\n";
	}
	close(INPUT);
	close(OUTPUT);
}