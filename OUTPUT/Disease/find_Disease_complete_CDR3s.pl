use warnings;
use strict;

#!/usr/bin/perl -w
use strict;
use warnings;
use Cwd;

my $dir = cwd;
$dir = "$dir";

my $output = 'Disease_Complete_CDR3s_all.txt';
open( INPUT, ">$output" );
close(INPUT);
my @filelist;
my $file;
my %answerhash;
my %answercounthash;
my %citationshash;
my %typehash;
opendir( DIR, $dir ) or die "can't opendir $dir: $!";
while ( defined( $file = readdir(DIR) ) )
{
	next if $file =~ /^\.\.?$/;                # skip . and ..
	next if $file =~ m/Decombinator_output/;
	next if $file =~ /tags/;
	next if $file =~ /Summary/;
	next if $file =~ /summary/;
	next if $file =~ /Combined/;
	next if $file =~ /analyze/;

	#next if $file =~ /$inputfile/;
	#next if $file =~ /CD8/;
	if ( $file =~ m/Disease_Overlap.txt/ )
	{
		push( @filelist, $file );    #read in all files from directory
		                             #print("$file\n");
	}
}
closedir(DIR);

for my $i ( 0 .. $#filelist )
{
	main( $filelist[$i] );
}

sub main
{
	my $totalcounter = 0;
	my $inputfile    = shift;
	my $realinput    = "$dir/$inputfile";
	$inputfile =~ s/Disease_Overlap.txt//;
	my $countcounter = 0;
	open( INPUT, $realinput ) or die;
	while ( my $line = <INPUT> )
	{
		$line =~ s/\n$//;
		my @currentline = split( /\s+/, $line );
		if ( $#currentline > 1 )
		{
			my $realkey = join( "\t",$currentline[0], $currentline[1], $currentline[2] );
			$answerhash{$realkey}++;
			$answercounthash{$realkey} += $currentline[$#currentline];
			push(@{$citationshash{$realkey}}, $inputfile);
			$typehash{$realkey} = $currentline[3]
		}

	}
	close(INPUT);

}

open( OUTPUT, ">$output" );
for my $key (keys %answerhash)
{
print OUTPUT "$key\t$answerhash{$key}\t$answercounthash{$key}\t$typehash{$key}\t";
for my $i (0..$#{$citationshash{$key}})
{
print OUTPUT "$citationshash{$key}->[$i]\t";
}
print OUTPUT "\n";
}
close(OUTPUT);
