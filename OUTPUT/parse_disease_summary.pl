#!/usr/bin/perl -w
use strict;
use warnings;
use Cwd;

my $dir = cwd;
$dir = "$dir/Disease";

my $output = 'Disease_overall_summary.txt';
open(INPUT,">$output");
close(INPUT);
my @filelist;
my $file;
opendir( DIR, $dir ) or die "can't opendir $dir: $!";
while ( defined( $file = readdir(DIR) ) ) {
	next if $file =~ /^\.\.?$/;                # skip . and ..
	next if $file =~ m/Decombinator_output/;
	next if $file =~ /tags/;
	next if $file =~ /Summary/;
	next if $file =~ /summary/;
	next if $file =~ /Combined/;
	next if $file =~ /analyze/;
        #next if $file =~ /$inputfile/;
	#next if $file =~ /CD8/;
	if ( $file =~ m/AA_UNIQUE_OUTPUT.txt/ ) {
		push( @filelist, $file );              #read in all files from directory
		                                       #print("$file\n");
	}
}
closedir(DIR);

for my $i (0..$#filelist)
{
    main($filelist[$i]);
}

sub main
{
    my $totalcounter = 0;
    my $inputfile = shift;
    my $realinput = "$dir/$inputfile";
    my $countcounter = 0;
    open(INPUT, $realinput) or die;
    while (my $line = <INPUT>) {
        $line =~ s/\n$//;
        my @currentline = split(/\s+/, $line);
        if ($#currentline > 1) {
            my $realkey = join( "\t", $currentline[0], $currentline[1], $currentline[2] );
        }
        
    }
    close(INPUT);
    open(OUTPUT, ">>$output");
    print OUTPUT "$inputfile\t$countcounter\t$totalcounter\n";
    close(OUTPUT);
}