#Script for analyzing the overlaps between LS_sample.

use warnings;
use strict;
use Cwd;
use List::Compare;

my $dir = cwd;
print "Welcome to find_LS_overlap.pl, a tool used to find the overlaps between the LS files in my AA_UNIQUE_OUTPUT files\n";
print "Currently operating in this directory: $dir\n";
my $permanent_output = 'LS_overlap_summary.txt';
open(INPUT, ">$permanent_output");
print INPUT "File_1\tFile_2\tAAs_File_1\tAAs_File_2\tCounts_File_1\tCounts_File_2\tAAs_Overlap\tCounts_Overlap_File1\tCounts_Overlap_File2\n";
close(INPUT);

opendir( DIR, $dir ) or die "can't opendir $dir: $!";
my $file;
my @filelist;
while ( defined( $file = readdir(DIR) ) ) {
	next if $file =~ /^\.\.?$/;                # skip . and ..
	next if $file =~ m/Decombinator_output/;
	next if $file =~ /tags/;
	if ( $file =~ m/.txt/ && $file =~ m/^L\d/) {
		push( @filelist, $file );              #read in all files from directory
		print("$file is being pushed to file list\n");
	}
}
closedir(DIR);

my @listone = qw(L1ACAA_UNIQUE_OUTPUT.txt L2ACAA_UNIQUE_OUTPUT.txt L3ACAA_UNIQUE_OUTPUT.txt L4ACAA_UNIQUE_OUTPUT.txt L5ACAA_UNIQUE_OUTPUT.txt L6ACAA_UNIQUE_OUTPUT.txt L7ACAA_UNIQUE_OUTPUT.txt L8ACAA_UNIQUE_OUTPUT.txt);
my @listtwo = qw(L1BCAA_UNIQUE_OUTPUT.txt L2BCAA_UNIQUE_OUTPUT.txt L3BCAA_UNIQUE_OUTPUT.txt L4BCAA_UNIQUE_OUTPUT.txt L5BCAA_UNIQUE_OUTPUT.txt L6BCAA_UNIQUE_OUTPUT.txt L7BCAA_UNIQUE_OUTPUT.txt L8BCAA_UNIQUE_OUTPUT.txt);

for my $i (0..$#listone)
{
	main($listone[$i], $listtwo[$i]);
}

sub main
{
	my $now = time;
	my $inputfileone = shift;
	my $inputfiletwo = shift;
	my $sub = substr($inputfileone, 0, 2) . ".txt";
	my $outputfile = "$dir/LS_overlap/$sub";
	print ("Current outputfile is $outputfile\n");
	open (OUTPUT, ">$outputfile") or die "Can't open '$outputfile' for reading + $!\n";;
	print OUTPUT "V_tag\tCDR3\tJ_tag\tFile_A_Count\tFile_B_Count\n";
	close(OUTPUT);
	my %hash_one;
	my %hash_two;
	my $AAs_one;
	my $counts_one;
	my $AAs_two;
	my $counts_two;
	open (INPUT1, $inputfileone);
	while (my $line = <INPUT1>)
	{
		$line =~ s/\n$//;
		my @currentline = split(/\s+/, $line);
		if ($#currentline > 0)
		{
		$AAs_one++;
		$counts_one += $currentline[3];
		my $realkey = join("\t",$currentline[0], $currentline[1], $currentline[2]);
		$hash_one{$realkey} = $currentline[3];
		}
	}
	close(INPUT1);
	
	open (INPUT2, $inputfiletwo);
	while (my $line = <INPUT2>)
	{
		$line =~ s/\n$//;
		my @currentline = split(/\t/, $line);
		if ($#currentline > 0)
		{
		$AAs_two++;
		$counts_two += $currentline[3];
		my $realkey = join("\t",$currentline[0], $currentline[1], $currentline[2]);
		$hash_two{$realkey} = $currentline[3];
		}
	}
	close(INPUT2);
	
	my @keys_one = keys %hash_one;
	my @keys_two = keys %hash_two;
	my $lc = List::Compare->new(\@keys_one, \@keys_two);
	my @intersection = $lc->get_intersection;
	my $AA_overlap;
	my $counts_overlap_1;
	my $counts_overlap_2;
	open (OUTPUT, ">$outputfile");
	for my $i (0..$#intersection)
	{
		print "Current intersection is $intersection[$i]\n";
		print "Hash one is $hash_one{$intersection[$i]}\n";
		print "Hash two is $hash_two{$intersection[$i]}\n";
		$AA_overlap++;
		my @to_print_key = split(/\t/, $intersection[$i]);
		for my $i (0..$#to_print_key)
		{
			print OUTPUT "$to_print_key[$i]\t";
		}
		print OUTPUT "$hash_one{$intersection[$i]}\t";
		print OUTPUT "$hash_two{$intersection[$i]}\n";
		$counts_overlap_1 += $hash_one{$intersection[$i]};
		$counts_overlap_2 += $hash_two{$intersection[$i]};
	}
	close(OUTPUT);
	open (OUTPUT3, ">>$permanent_output");
	print OUTPUT3 "$inputfileone\t$inputfiletwo\t$AAs_one\t$AAs_two\t$counts_one\t$counts_two\t$AA_overlap\t$counts_overlap_1\t$counts_overlap_2\n";
	close(OUTPUT3);
}
