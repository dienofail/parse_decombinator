#This script is designed to count the number of unique sequences across CD4/CD8/LS subsets

use warnings;
use strict;
use Cwd;
my $dir = cwd;


my @listone = qw(CD8_M1a8-7_2nd_run_F6-5AA_UNIQUE_OUTPUT.txt
  CD8_M2a8-7_F5-1AA_UNIQUE_OUTPUT.txt
  CD8_M3a8-7_F6-1AA_UNIQUE_OUTPUT.txt
  CD8_O1a8-7_F5-5AA_UNIQUE_OUTPUT.txt
  CD8_O2a8-7_F5-2AA_UNIQUE_OUTPUT.txt
  CD8_Y1a8-7_F5-6AA_UNIQUE_OUTPUT.txt
  CD8_Y2a8-7_F5-7AA_UNIQUE_OUTPUT.txt
  CD8_Y3a8-7_F6-2AA_UNIQUE_OUTPUT.txt
);
my @listtwo = qw(
  M1CAA_UNIQUE_OUTPUT.txt
  M1_4_3e8CAA_UNIQUE_OUTPUT.txt
  M2CAA_UNIQUE_OUTPUT.txt
  M3b4-7_F4-6AA_UNIQUE_OUTPUT.txt
  O1CAA_UNIQUE_OUTPUT.txt
  O2b4-7_F4-3AA_UNIQUE_OUTPUT.txt
  O1_4_3e8CAA_UNIQUE_OUTPUT.txt
  Y1_4CAA_UNIQUE_OUTPUT.txt
  Y1_4_8CAA_UNIQUE_OUTPUT.txt
  Y2b4-7_F4-5AA_UNIQUE_OUTPUT.txt
  Y3CAA_UNIQUE_OUTPUT.txt
);
my @listthree =
qw(L1ACAA_UNIQUE_OUTPUT.txt
L1BCAA_UNIQUE_OUTPUT.txt
L3ACAA_UNIQUE_OUTPUT.txt
L3BCAA_UNIQUE_OUTPUT.txt
L4ACAA_UNIQUE_OUTPUT.txt
L4BCAA_UNIQUE_OUTPUT.txt
L5ACAA_UNIQUE_OUTPUT.txt
L5BCAA_UNIQUE_OUTPUT.txt
L6ACAA_UNIQUE_OUTPUT.txt
L6BCAA_UNIQUE_OUTPUT.txt
L7ACAA_UNIQUE_OUTPUT.txt
L7BCAA_UNIQUE_OUTPUT.txt
);


#main subrountine will take in an arrayref of a list of files

main(\@listone);
main(\@listtwo);
main(\@listthree);
my %totalhash;
sub main
{
	my $totalcounts = 0;
	my $inputarrayref = shift;
	my @array = @{$inputarrayref};
	my %answerhash;
	for my $i (0..$#array)
	{
		open(INPUT, "$array[$i]");
		while (my $line = <INPUT>)
		{
			$line =~ s/\n$//;
			my @currentline = split(/\s+/,$line);
			my $realkey = join( "\t", $currentline[0], $currentline[1], $currentline[2] );
			$answerhash{$realkey} += $currentline[3];
			$totalcounts += $currentline[3];
			$totalhash{$realkey} += $currentline[3];
		}
		close(INPUT);
	}
	my @keys = keys %answerhash;
	my $numkeys = $#keys + 1;
	print "My keys is $numkeys and my counts is $totalcounts\n";
}
my @totalkeys = keys %totalhash;
my $total = scalar(@totalkeys);
print "My totaloutput is $total";