use warnings;
use strict;
use List::Compare;
my $inputone = 'all_overlaps.txt';
my %overlaphash;
open(INPUT, "$inputone");
while (my $line = <INPUT>)
{
	$line =~ s/\n$//;
	my @currentline = split(/\t/, $line);
	if ($#currentline > 2)
	{
	my $hashvalue = join("\t", $currentline[0], $currentline[1], $currentline[2]);
	my $hashkey = $currentline[3];
	push(@{$overlaphash{$hashkey}}, $hashvalue);
	}
}
close(INPUT);
my $file;
my @filelist;
use Cwd;
my $dir = cwd;
opendir( DIR, $dir ) or die "can't opendir $dir: $!";
while ( defined( $file = readdir(DIR) ) ) {
	next if $file =~ /^\.\.?$/;                # skip . and ..
	next if $file =~ m/Decombinator_output/;
	next if $file =~ /tags/;
	next if $file =~ /Summary/;
	next if $file =~ /summary/;
	next if $file =~ /Combined/;
	next if $file =~ /analyze/;
	#next if $file =~ /CD8/;
	if ( $file =~ m/AA_UNIQUE_OUTPUT.txt/ ) {
		push( @filelist, $file );              #read in all files from directory
		                                       #print("$file\n");
	}
}
closedir(DIR);

for my $i ( 0 .. $#filelist ) {
	main( $filelist[$i] );
}

sub main
{
	my $inputfile = shift;
	my $outputfile = $inputfile;
	$outputfile =~ s/AA_UNIQUE_OUTPUT.txt/NOT_OVERLAPPING.txt/;
	$outputfile = "$dir/Not_Overlapping/$outputfile";
	my @overlaps; 
	for my $key (keys %overlaphash)
	{
		if ($inputfile =~ /$key/)
		{
			@overlaps = $overlaphash{$key};
		}
	}
	my @currentkeys;
	open(INPUT, "$inputfile");
	while (my $line = <INPUT>)
	{
		$line =~ s/\n$//;
		my @currentline = split (/\t/,$line);
		if ($#currentline > 1)
		{
			my $realkey = join("\t", $currentline[0], $currentline[1] ,$currentline[2]);
			push(@currentkeys, $realkey);
		}
	}
	close(INPUT);
	my $lc = List::Compare->new(\@currentkeys, \@overlaps);
	my @intersection = $lc->get_intersection;
	my @currentonly = $lc->get_Lonly;
	open(OUTPUT, ">$outputfile");
	for my $i (0..$#currentonly)
	{
		print OUTPUT "$currentonly[$i]\n";
	}
	close(OUTPUT);
}

