use warnings;
use strict;
use List::MoreUtils qw(uniq);
use Cwd;
my $dir = cwd;
my $currentdir = $dir;
my $file;
my $permanent_output = 'analyze_overlaps_output.txt';
open(INPUT, ">$permanent_output") or die "Can't open '$permanent_output' for reading + $!\n";
print INPUT "File\tTotal_Overlapping_AAs\tTotal_Unique_CDR3s\tTwo\tThree\tFour\tFive\tSix_or_more\n";
close(INPUT);
my @filelist;
opendir( DIR, $currentdir ) or die "can't opendir $dir: $!";
while ( defined( $file = readdir(DIR) ) ) {
	next if $file =~ /^\.\.?$/;                # skip . and ..
	next if $file =~ m/Decombinator_output/;
	next if $file =~ /tags/;
	next if $file =~ /find_unique_summary/;
	next if $file =~ m/Summary/;
	if ( $file =~ m/.txt/ ) {
		push( @filelist, $file );              #read in all files from directory
		                                       #print("$file\n");
	}
}
closedir(DIR);

for my $i (0..$#filelist)
{
	main($filelist[$i]);
}

sub main
{
	my $inputfileone = shift;
	my $outputfile = $inputfileone;
	$outputfile =~ s/_Overlaps.txt/_ANALYZED_OVERLAPs.txt/;
	$outputfile = "$currentdir/Analyzed/$outputfile";
	my %hash;
	my $count;
	my $two = 0;
	my $three = 0;
	my $four = 0;
	my $five = 0;
	my $sixormore = 0;
	my $keycount = 0;
	open (INPUT, "$inputfileone");
	while (my $line = <INPUT>)
	{
		$line =~ s/\n$//;
		my @currentline = split(/\t/, $line);
		if ($#currentline > 1)
		{
			$count++;
			my $realkey = join("\t", $currentline[1] ,$currentline[2]);
			push(@{$hash{$realkey}}, $currentline[0]);
		}
	}
	close(INPUT);
	open(OUTPUT, ">$outputfile") or die "Can't open '$outputfile' for reading + $!\n";
	for my $key (keys %hash)
	{
		$keycount++;
		my $currentlength = $#{$hash{$key}} + 1;
		print OUTPUT "$key\t$currentlength\n";
		if ($currentlength == 2)
		{
			$two++;
		}
		if ($currentlength == 3)
		{
			$three++;
		}
		if ($currentlength == 4)
		{
			$four++;
		}
		if ($currentlength == 5)
		{
			$five++;
		}
		if ($currentlength >= 6)
		{
			$sixormore++;
		}
	}
	close(OUTPUT);
	
	open(OUTPUT, ">>$permanent_output");
	print OUTPUT "$inputfileone\t$count\t$keycount\t$two\t$three\t$four\t$five\t$sixormore\n";
	close(OUTPUT);
}