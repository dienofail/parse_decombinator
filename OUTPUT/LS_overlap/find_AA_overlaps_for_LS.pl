use warnings;
use strict;
use Cwd;
use List::MoreUtils qw/ uniq /;

my $dir = getcwd();
print("$dir\n");
my $permanent_output = "$dir/Overlaps2/Summary.txt";
open( OUTPUT, ">$permanent_output" );
print OUTPUT "Filename\tOver_laps\tTotal_Unique_Keys\n";
close(OUTPUT);
opendir( DIR, $dir ) or die "can't opendir $dir: $!";
my $file;
my @filelist;

while ( defined( $file = readdir(DIR) ) ) {
	next if $file =~ /^\.\.?$/;                # skip . and ..
	next if $file =~ m/Decombinator_output/;
	next if $file =~ /tags/;
	if ( $file =~ m/.txt/ ) {
		push( @filelist, $file );              #read in all files from directory
		print("$file\n");
	}
}
closedir(DIR);
for my $i(0..$#filelist)
{
	main($filelist[$i]);
}

sub main {
	my %uniquehash;
	my %counthash;
	my $inputfile = shift;
	print("Currently on $inputfile\n");
	my $uniquefile = $inputfile;
	$uniquefile =~ s/.txt/_Overlaps.txt/;
	print ("My uniquefile name is $uniquefile\n");
	my $keycount = 0;
	my $overlapscount = 0;
	open( INPUT, "$inputfile" );
	while ( my $line = <INPUT> ) {
		$line =~ s/\n$//;
		my @currentline = split( /\t/, $line );
		if ( $#currentline > 2 ) {
			
			$keycount++;

			#print ("$currentline[0], $currentline[7], $currentline[1]\n");
			my $realkey = join( ',', $currentline[1], $currentline[2] );
			#print("Currently setting my realkey to CDR3 AA sequence\n");

			#print("$realkey\n");
			my @answerarray; 
			push(@answerarray, $currentline[0]);
			push(@answerarray, $currentline[3]);
			push( @{ $uniquehash{$realkey} }, \@answerarray);
			#$uniquehash{$realkey} = @answerarray;
			#my $secondkey =
			  #join( ',', $currentline[0], $currentline[1], $currentline[2] );
			#$counthash{$realkey} = $currentline[3];
		}
	}
	close(INPUT);

	my $outputfile_name = "$dir/Overlaps2/$uniquefile";
	open (OUTPUT, ">$outputfile_name");
	my @toprintkeys;
	for my $key ( keys %uniquehash ) {
		my @tochecklist;
		for my $i (0..$#{ $uniquehash{$key} })
		{
			push(@tochecklist, $uniquehash{$key}->[$i]->[0]);
		}
		#my @tochecklist = @{ $uniquehash{$key} };
		my @splitkeys = split (/,/, $key);
		my @uniqlist    = uniq @tochecklist;
		if ( $#uniqlist >= 1 ) {
			$overlapscount++;
			for my $i ( 0 .. $#uniqlist ) {
#				my $combined_name = join( ',', $uniqlist[$i], $splitkeys[0], $splitkeys[1]);
#				push( @toprintkeys, $combined_name );
#				print(
#"I have detected a hit, and my new combined name is $combined_name\n"
				print OUTPUT "$uniquehash{$key}->[$i]->[0]\t$splitkeys[0]\t$splitkeys[1]\t$uniquehash{$key}->[$i]->[1]\n";
			}
		}
	}
	close (OUTPUT);
#	open (OUTPUT, ">$outputfile_name");
#	for my $i (0..$#toprintkeys)
#	{
#		my $key = $toprintkeys[$i];
#		my @splitkey = split(',', $key); 
#		for my $j (0..$#splitkey)
#		{
#			print OUTPUT "$splitkey[$j]\t";
#		}
#		print OUTPUT "$counthash{$key}\n";
#	}
#	close (OUTPUT);
	
	open (OUTPUT, ">>$permanent_output");
	print OUTPUT "$inputfile\t$overlapscount\t$keycount\n";
	close(OUTPUT);
}