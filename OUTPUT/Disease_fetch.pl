#given a list of complete CDR3s to match, find the exact matches in each of the files and print the counts
use warnings; 
use strict;
use Cwd;
my $disease = 'Disease_Complete_CDR3s_all.txt';
my $output = 'Disease_reannotated_10_31.txt';
my %diseasehash;
my %diseasedata;
open (INPUT, "$disease");
while (my $line = <INPUT>)
{
	$line =~ s/\n$//;
	my @currentline = split(/\t/, $line);
	my $realkey = join( "\t",$currentline[0], $currentline[1], $currentline[2] );
	$diseasehash{$realkey} = $currentline[3];
	$diseasedata{$realkey} = {};
}
close(INPUT);
my @diseasekeys = keys %diseasehash;

my $dir = cwd;
my @filelist;
my $file;
my %fileindexarray;
opendir( DIR, $dir ) or die "can't opendir $dir: $!";
while ( defined( $file = readdir(DIR) ) ) {
	next if $file =~ /^\.\.?$/;                # skip . and ..
	next if $file =~ m/Decombinator_output/;
	next if $file =~ /tags/;
	next if $file =~ /Summary/;
	next if $file =~ /summary/;
	next if $file =~ /Combined/;
	next if $file =~ /analyze/;
    next if $file =~ /GBSET/;
	#next if $file =~ /CD8/;
	if ( $file =~ m/AA_UNIQUE_OUTPUT.txt/ ) {
		push( @filelist, $file );              #read in all files from directory
		print("$file\n");
	}
}
closedir(DIR);

for my $i (0..$#filelist)
{
	main($filelist[$i]);
	my $currentfile = $filelist[$i];
	$currentfile =~ s/AA_UNIQUE_OUTPUT.txt//;
	$fileindexarray{$currentfile} = $i;
}
my @filekeys = keys %fileindexarray;
open(OUTPUT, ">$output");
print OUTPUT "V\tCDR3\tJ\tDisease_Annotation";
for my $i (0..$#filekeys)
{
	print OUTPUT "\t$filekeys[$i]";
}
print OUTPUT "\n";

for my $i (0..$#diseasekeys)
{
	print OUTPUT "$diseasekeys[$i]\t$diseasehash{$diseasekeys[$i]}";
	for my $j (0..$#filekeys)
	{
		if (defined $diseasedata{$diseasekeys[$i]}{$filekeys[$j]})
		{
			print OUTPUT "\t$diseasedata{$diseasekeys[$i]}{$filekeys[$j]}";
		}
		else
		{
			print OUTPUT "\tNaN";
		}
	}
	print OUTPUT "\n";
}

close(OUTPUT);



sub main
{
	my $input = shift;
	my $now = time;
	print "Currently on file $input";
	my $copyofinput = $input;
	$copyofinput =~ s/AA_UNIQUE_OUTPUT.txt//;
	open(INPUT, "$input");
	while (my $line = <INPUT>)
	{
		$line =~ s/\n$//;
		my @currentline = split(/\t/, $line);
		for my $i (0..$#diseasekeys)
		{
			my $currentdiseasekey = $diseasekeys[$i];
			my @currentdiseasekey = split(/\t/, $currentdiseasekey);
			my $tomatch = $currentdiseasekey[1];
			if ($tomatch =~ /$currentline[1]/ && $currentline[0]==$currentdiseasekey[0] && $currentline[2]==$currentdiseasekey[2])
			{
				my $topush = join( ',', $currentline[1],  $currentline[3]);
				$diseasedata{$currentdiseasekey}{$copyofinput} = $topush;
				print "Currently adding $diseasedata{$currentdiseasekey}{$copyofinput} to hash\n";
			}
		}
	}
	close(INPUT);
	$now = time - $now;
	# Print runtime #
	printf("\n\nTotal running time: %02d:%02d:%02d\n\n", int($now / 3600), int(($now % 3600) / 60), 
	int($now % 60));
}

