use warnings;
use strict;
use Cwd;
my $dir = cwd;


opendir( DIR, $dir ) or die "can't opendir $dir: $!";
my $file;
my @filelist;
while ( defined( $file = readdir(DIR) ) ) {
	next if $file =~ /^\.\.?$/;                # skip . and ..
	next if $file =~ m/Decombinator_output/;
	next if $file =~ /tags/;
	if ( $file =~ m/AA_UNIQUE_OUTPUT.txt/ ) {
		push( @filelist, $file );              #read in all files from directory
		print("$file\n");
	}
}
closedir(DIR);

for my $i ( 0 .. $#filelist ) {
	main( $filelist[$i] );
}

sub main
{
	my $inputfile = shift;
	my $totalcount = 0;
	my $count = 0;
	my $averagecount;
	open(INPUT, "$inputfile");
	while (my $line = <INPUT>)
	{
		$line =~ s/\n$//;
		my @currentline = split(/\t/, $line);
		if ($#currentline > 2)
		{
		$totalcount += $currentline[3];
		$count++;
		}
	}
	$averagecount = $totalcount / $count;
	print "$inputfile\t$averagecount\t$totalcount\n";
}