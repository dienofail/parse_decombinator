use warnings;
use strict;
my $permanent_output_CD4 = 'CD4_within_overlap_data.txt';
my $permanent_output_CD8 = 'CD8_within_overlap_data.txt';

my %cd4hash;
my %cd8hash;
my @listone = qw(CD8_M1a8-7_2nd_run_F6-5AA_UNIQUE_OUTPUT.txt
  CD8_M2a8-7_F5-1AA_UNIQUE_OUTPUT.txt
  CD8_M3a8-7_F6-1AA_UNIQUE_OUTPUT.txt
  CD8_O1a8-7_F5-5AA_UNIQUE_OUTPUT.txt
  CD8_O2a8-7_F5-2AA_UNIQUE_OUTPUT.txt
  CD8_Y1a8-7_F5-6AA_UNIQUE_OUTPUT.txt
  CD8_Y2a8-7_F5-7AA_UNIQUE_OUTPUT.txt
  CD8_Y3a8-7_F6-2AA_UNIQUE_OUTPUT.txt
);
my @listtwo = qw(
  M1CAA_UNIQUE_OUTPUT.txt
  M2CAA_UNIQUE_OUTPUT.txt
  M3b4-7_F4-6AA_UNIQUE_OUTPUT.txt
  O1CAA_UNIQUE_OUTPUT.txt
  O2b4-7_F4-3AA_UNIQUE_OUTPUT.txt
  Y1_4CAA_UNIQUE_OUTPUT.txt
  Y2b4-7_F4-5AA_UNIQUE_OUTPUT.txt
  Y3CAA_UNIQUE_OUTPUT.txt
);


for my $i (0..$#listone)
{
	cd8main($listone[$i]);
	cd4main($listtwo[$i]);
}

analyze_hash(\%cd4hash, $permanent_output_CD4);
analyze_hash(\%cd8hash, $permanent_output_CD8);
sub cd4main
{
	my $inputfile = shift;
	open (INPUT, $inputfile);
	while (my $line = <INPUT>)
	{
		$line =~ s/\n$//;
		my @currentline = split(/\t/, $line);
		my $realkey = join("\t",$currentline[0], $currentline[1], $currentline[2]);
		push(@{$cd4hash{$realkey}}, $currentline[3]);
	}
	close(INPUT);
}

sub cd8main
{
	my $inputfile = shift;
	open (INPUT, $inputfile);
	while (my $line = <INPUT>)
	{
		$line =~ s/\n$//;
		my @currentline = split(/\t/, $line);
		my $realkey = join("\t",$currentline[0], $currentline[1], $currentline[2]);
		push(@{$cd8hash{$realkey}}, $currentline[3]);
	}
	close(INPUT);
}
sub analyze_hash
{
	my $hashref = shift;
	my $outputfile = shift;
	my %hash = %$hashref;
	my $one;
	my $onecount;
	my $two;
	my $twocount;
	my $three;
	my $threecount;
	my $four;
	my $fourcount;
	my $five;
	my $fivecount;
	my $six;
	my $sixcount;
	my $seven;
	my $sevencount;
	my $eight;
	my $eightcount;
	my $totalunique;
	my $totalcount;
	
	for my $key (keys %hash)
	{
		$totalunique++;
		my @currentarray = @{$hash{$key}};
		my $currentcount = scalar(@currentarray);
		my $currenttotal;
		for my $i (0..$#currentarray)
		{
			$currenttotal += $currentarray[$i];
		}
		$totalcount += $currenttotal;
		if ($currentcount == 1)
		{
			$one++;
			$onecount += $currenttotal;
		}
		if ($currentcount == 2)
		{
			$two++;
			$twocount += $currenttotal;
		}
		if ($currentcount == 3)
		{
			$three++;
			$threecount += $currenttotal;
		}
		if ($currentcount == 4)
		{
			$four++;
			$fourcount += $currenttotal;
		}
		if ($currentcount == 5)
		{
			$five++;
			$fivecount += $currenttotal;
		}
		if ($currentcount == 6)
		{
			$six++;
			$sixcount += $currenttotal;
		}
		if ($currentcount == 7)
		{
			$seven++;
			$sevencount += $currenttotal;
		}
		if ($currentcount == 8)
		{
			$eight++;
			$eightcount += $currenttotal;
		}
	}	
	
	open (OUTPUT, ">$outputfile");
	print OUTPUT "Shared\tUnique_V_CDR3_J\tTotal\n";
	print OUTPUT "8\t$eight\t$eightcount\n";
	print OUTPUT "7\t$seven\t$sevencount\n";
	print OUTPUT "6\t$six\t$sixcount\n";
	print OUTPUT "5\t$five\t$fivecount\n";
	print OUTPUT "4\t$four\t$fourcount\n";
	print OUTPUT "3\t$three\t$threecount\n";
	print OUTPUT "2\t$two\t$twocount\n";
	print OUTPUT "1\t$one\t$onecount\n";
	print OUTPUT "Total\t$totalunique\t$totalcount\n";
	close (OUTPUT);
}

