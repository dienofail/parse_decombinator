use strict;
use warnings;
use Cwd;
my $dir = getcwd();
print("$dir\n");

opendir( DIR, $dir ) or die "can't opendir $dir: $!";
my $file;
my @filelist;
while ( defined( $file = readdir(DIR) ) ) {
	next if $file =~ /^\.\.?$/;                # skip . and ..
	next if $file =~ m/Decombinator_output/;
	next if $file =~ /tags/;
	if ($file =~ /.txt.txt$/)
	{
		unlink $file;
	}
	if ( $file =~ m/AA_UNIQUE_OUTPUT.txt/ ) {
		push( @filelist, $file );              #read in all files from directory
		print("$file\n");
	}
}
closedir(DIR);
