use warnings;
use strict;
my $output1 = 'CD8_overlapping_all.txt';
my $output2 = 'CD4_overlapping_all.txt';
my @listone = qw(CD8_M1a8-7_2nd_run_F6-5
  CD8_M2a8-7_F5-1
  CD8_M3a8-7_F6-1
  CD8_O1a8-7_F5-5
  CD8_O2a8-7_F5-2
  CD8_Y1a8-7_F5-6
  CD8_Y2a8-7_F5-7
  CD8_Y3a8-7_F6-2
);
my @listtwo = qw(
  M1C
  M2C
  M3b4-7_F4-6
  O1C
  O2b4-7_F4-3
  Y1_4C
  Y2b4-7_F4-5
  Y3C
);

use Cwd;
my $dir = cwd;

my @filelist1;
my @filelist2;
my $file;
opendir( DIR, $dir ) or die "can't opendir $dir: $!";
while ( defined( $file = readdir(DIR) ) ) {
	next if $file =~ /^\.\.?$/;                # skip . and ..
	next if $file =~ m/Decombinator_output/;
	next if $file =~ /tags/;
	next if $file =~ /Summary/;
	next if $file =~ /summary/;
	next if $file =~ /Combined/;
	next if $file =~ /analyze/;
	#next if $file =~ /CD8/;
	for my $i (0..$#listone)
	{
		if ( $file =~ /$listone[$i]/ && $file =~ /OVERLAP.txt/ ) {
			push( @filelist1, $file );              #read in all files from directory
			                                       print("$file\n");
		}
	}
		for my $i (0..$#listtwo)
	{
		if ( $file =~ /$listtwo[$i]/  && $file =~ /OVERLAP.txt/ ) {
			push( @filelist2, $file );              #read in all files from directory
			                                       print("$file\n");
		}
	}
}
closedir(DIR);

for my $i (0..$#filelist1)
{
	main($filelist1[$i]);
	main2($filelist2[$i]);
}
my %cd8;
my %cd4;

sub main
{
	my $inputfile = shift;
	open( INPUT, "$inputfile" );
	while ( my $line = <INPUT> ) {
		$line =~ s/\n$//;
		my @currentline = split( /\t/, $line );
		if ( $#currentline > 1 ) {
			#print ("$currentline[0], $currentline[7], $currentline[1]\n");
			my $realkey = join( "\t", $currentline[0], $currentline[1], $currentline[2] );
			#print("Currently setting my realkey to CDR3 AA sequence\n")
			#print("$realkey\n");
			my @answerarray; 
			$cd8{$realkey} = $currentline[3];
			#my $secondkey =
			  #join( ',', $currentline[0], $currentline[1], $currentline[2] );
			#$counthash{$realkey} = $currentline[3];
		}
	}
	close(INPUT);
}


sub main2
{
	my $inputfile = shift;
	open( INPUT, "$inputfile" );
	while ( my $line = <INPUT> ) {
		$line =~ s/\n$//;
		my @currentline = split( /\t/, $line );
		if ( $#currentline > 1 ) {
			#print ("$currentline[0], $currentline[7], $currentline[1]\n");
			my $realkey = join( "\t", $currentline[0], $currentline[1], $currentline[2] );
			#print("Currently setting my realkey to CDR3 AA sequence\n")
			#print("$realkey\n");
			my @answerarray; 
			$cd4{$realkey} = $currentline[3];
			#my $secondkey =
			  #join( ',', $currentline[0], $currentline[1], $currentline[2] );
			#$counthash{$realkey} = $currentline[3];
		}
	}
	close(INPUT);
}

open(OUTPUT, ">$output1");
for my $key (keys %cd8)
{
	print OUTPUT "$key\n";
}
close(OUTPUT);

open(OUTPUT, ">$output2");
for my $key (keys %cd4)
{
	print OUTPUT "$key\n";
}
close(OUTPUT);