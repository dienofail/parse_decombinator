#File for analyzing overlaps between the same CDR3-J (but different V) sequences from my decombinatoroutputs.
#Will output the different distributions of V-usage for the same CDR3-J.
use warnings;
use strict;
use List::MoreUtils qw(uniq);
use Cwd;
my $dir = cwd;
my $currentdir = $dir;
my $file;
my $permanent_output = 'analyze_overlaps_output.txt';
#my $six_or_more_file = 'six_or_more_summary.txt';
#open (INPUT, ">$six_or_more_file");
#print INPUT "CDR3\tJ_gene\tInput_file\tDuplicity\tV_usage\n";
#close(INPUT);
my %toprinthash;
open(INPUT, ">$permanent_output") or die "Can't open '$permanent_output' for reading + $!\n";
print INPUT "File\tTotal_Overlapping_AAs\tTotal_Unique_CDR3s\tAverage_Multiplicity\tAverage_Count_of_Two_or_More\tTwo_or_more_Total\tFile_Average\tTwo\tThree\tFour\tFive\tSix\tSeven\tEight\tNine\tTen_or_more\n";
close(INPUT);
my @filelist;
opendir( DIR, $currentdir ) or die "can't opendir $dir: $!";
while ( defined( $file = readdir(DIR) ) ) {
	next if $file =~ /^\.\.?$/;                # skip . and ..
	next if $file =~ m/Decombinator_output/;
	next if $file =~ /tags/;
	next if $file =~ /find_unique_summary/;
	next if $file =~ /Summary/;
	next if $file =~ /six/;
	next if $file =~ /summary/;
	next if $file =~ /Total/;
	next if $file =~ /output/;
	if ( $file =~ m/AA_OVERLAP.txt/ ) {
		push( @filelist, $file );              #read in all files from directory
		                                       #print("$file\n");
	}
}
closedir(DIR);

for my $i (0..$#filelist)
{
	main($filelist[$i]);
}

sub main
{
	my $inputfileone = shift;
	my $outputfile = $inputfileone;
	$outputfile =~ s/AA_OVERLAP.txt/ANALYZED_OVERLAP.txt/;
	$outputfile = "$currentdir/Analyzed/$outputfile";
	my %hash;
	my $count;
	my $two = 0;
	my $three = 0;
	my $four = 0;
	my $five = 0;
	my $six = 0;
	my $seven = 0;
	my $eight = 0;
	my $nine = 0;
	my $ten = 0;
	my $keycount = 0;
	my $averageunique;
	my $totalcount;
	my $averagecount;
	my %counthash;
	my $filetotal;
	my $fileaverage;
	open (INPUT, "$inputfileone");
	while (my $line = <INPUT>)
	{
		$line =~ s/\n$//;
		my @currentline = split(/\t/, $line);
		if ($#currentline > 1)
		{
			$count++;
			my $realkey = join("\t", $currentline[1] ,$currentline[2]);
			push(@{$hash{$realkey}}, $currentline[0]);
			$counthash{$realkey} += $currentline[3];
			$filetotal += $currentline[3];
		}
	}
	$fileaverage = $filetotal/$count;
	close(INPUT);
	open(OUTPUT, ">$outputfile") or die "Can't open '$outputfile' for reading + $!\n";
	for my $key (keys %hash)
	{
		$keycount++;
		my $currentlength = $#{$hash{$key}} + 1;
		print OUTPUT "$key\t$currentlength\n";
		next if ($currentlength < 1);
		if ($currentlength >= 2)
		{
			$averageunique += $currentlength;
			$totalcount += $counthash{$key};
		}
		if ($currentlength == 2)
		{
			$two++;
		}
		elsif ($currentlength == 3)
		{
			$three++;
		}
		elsif ($currentlength == 4)
		{
			$four++;
		}
		elsif ($currentlength == 5)
		{
			$five++;
		}
		elsif ($currentlength == 6)
		{
			$six++;
			#$toprinthash{$key} = $inputfileone;
		}
		elsif ($currentlength == 7)
		{
			$seven++;
			#$toprinthash{$key} = $inputfileone;
		}
		elsif ($currentlength == 8)
		{
			$eight++;
			#$toprinthash{$key} = $inputfileone;
		}
		elsif ($currentlength == 9)
		{
			$nine++;
			#$toprinthash{$key} = $inputfileone;
		}
		elsif ($currentlength >= 10)
		{
			$ten++;
			my @toprintarray;
			push(@toprintarray, $inputfileone);
			push(@toprintarray, $currentlength);
			for my $i (0..$#{$hash{$key}})
			{
				push(@toprintarray, $hash{$key}->[$i]);
			}
			$toprinthash{$key} = \@toprintarray;
		}
	}
	close(OUTPUT);
	$averageunique = $averageunique/$keycount;
	$averagecount = $totalcount/$keycount;
	open(OUTPUT, ">>$permanent_output");
	print "$inputfileone\t$totalcount\n";
	print OUTPUT "$inputfileone\t$count\t$keycount\t$averageunique\t$averagecount\t$totalcount\t$fileaverage\t$two\t$three\t$four\t$five\t$six\t$seven\t$eight\t$nine\t$ten\n";
	close(OUTPUT);

#	for my $key (keys %toprinthash)
#	{
#		open(OUTPUT, ">>$six_or_more_file");
#		print OUTPUT "$key\t";
#		for my $i (0..$#{$toprinthash{$key}})
#		{
#			print OUTPUT "$toprinthash{$key}->[$i]\t";
#		}
#		print OUTPUT "\n";
#		close(OUTPUT);
#	}
}