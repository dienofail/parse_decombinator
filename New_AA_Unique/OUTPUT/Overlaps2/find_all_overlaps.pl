#File for analyzing overlaps between the same CDR3-J (but different V) sequences from my decombinatoroutputs.
#Will output the different distributions of V-usage for the same CDR3-J.
use warnings;
use strict;
use List::MoreUtils qw(uniq);
use Cwd;
my $dir = cwd;
my $currentdir = $dir;
my $file;
my $permanent_output = 'all_overlaps.txt';
#my $six_or_more_file = 'six_or_more_summary.txt';
#open (INPUT, ">$six_or_more_file");
#print INPUT "CDR3\tJ_gene\tInput_file\tDuplicity\tV_usage\n";
#close(INPUT);
my %toprinthash;
open(INPUT, ">$permanent_output") or die "Can't open '$permanent_output' for reading + $!\n";
close(INPUT);
my @filelist;
opendir( DIR, $currentdir ) or die "can't opendir $dir: $!";
while ( defined( $file = readdir(DIR) ) ) {
	next if $file =~ /^\.\.?$/;                # skip . and ..
	next if $file =~ m/Decombinator_output/;
	next if $file =~ /tags/;
	next if $file =~ /find_unique_summary/;
	next if $file =~ /Summary/;
	next if $file =~ /six/;
	next if $file =~ /summary/;
	next if $file =~ /Total/;
	next if $file =~ /output/;
	if ( $file =~ m/.txt/ ) {
		push( @filelist, $file );              #read in all files from directory
		                                       #print("$file\n");
	}
}
closedir(DIR);

for my $i (0..$#filelist)
{
	main($filelist[$i]);
}
my %hash;
sub main
{
	my $inputfileone = shift;
	my $outputfile = $inputfileone;
	$outputfile =~ s/AA_OVERLAP.txt/ANALYZED_OVERLAP.txt/;
	$outputfile = "$currentdir/Analyzed/$outputfile";
	my $copyofinput = $inputfileone;
	$copyofinput =~ s/AA_OVERLAP.txt//;
	#my %hash;
	my $count;
	open (INPUT, "$inputfileone");
	while (my $line = <INPUT>)
	{
		$line =~ s/\n$//;
		my @currentline = split(/\t/, $line);
		if ($#currentline > 1)
		{
			$count++;
			my $realkey = join("\t", $currentline[0], $currentline[1] ,$currentline[2]);
			push(@{$hash{$realkey}}, $copyofinput);
			#$hash{$realkey} += $currentline[3];
			#$filetotal += $currentline[3];
		}
	}
	close(INPUT);
	open(OUTPUT, ">>$permanent_output");
	for my $key (keys %hash)
	{
		print OUTPUT "$key\t";
		for my $i (0..$#{$hash{$key}})
		{
			print OUTPUT "$hash{$key}->[$i]\t";
		}
		print OUTPUT "\n";
	}
	close(OUTPUT);

#	for my $key (keys %toprinthash)
#	{
#		open(OUTPUT, ">>$six_or_more_file");
#		print OUTPUT "$key\t";
#		for my $i (0..$#{$toprinthash{$key}})
#		{
#			print OUTPUT "$toprinthash{$key}->[$i]\t";
#		}
#		print OUTPUT "\n";
#		close(OUTPUT);
#	}
}