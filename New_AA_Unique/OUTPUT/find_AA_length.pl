use strict;
use warnings;
use Cwd;
my $dir = getcwd();
print("$dir\n");
my $permanent_output = "$dir/Length_Counts/Length_Summary.txt";
open (OUTPUT, ">$permanent_output");
print OUTPUT "File_Name\t" . '1' . "\t" . '1count' . "\t" . '2<x<10' . "\t" . '2<x<10count' . "\t" . '10<x<100' . "\t" . '10<x<100count' . "\t" . '100<x<1000' ."\t" . '100<x<1000count' ."\t" . '1000<x<2000' . "\t" . '1000<x<2000count' . "\t" . '2000<x<3000'. "\t" . '2000<x<3000count' ."\t" . '3000<x' ."\t" . '3000<xcount' . "\tUnique_Count\tTotal_Count\n";
close(OUTPUT);
opendir( DIR, $dir ) or die "can't opendir $dir: $!";
my $file;
my @filelist;
while ( defined( $file = readdir(DIR) ) ) {
	next if $file =~ /^\.\.?$/;                # skip . and ..
	next if $file =~ m/Decombinator_output/;
	next if $file =~ /tags/;
	if ( $file =~ m/AA_UNIQUE_OUTPUT.txt/ ) {
		push( @filelist, $file );              #read in all files from directory
		print("$file\n");
	}
}
closedir(DIR);

for my $i ( 0 .. $#filelist ) {
	main( $filelist[$i] );
}

sub main {
	my %uniquehash;
	my %counthash;
	my $inputfile = shift;
	print("Currently on $inputfile\n");
	my $uniquefile = $inputfile;
	$uniquefile =~ s/AA_UNIQUE_OUTPUT.txt/AA_LENGTH_COUNT.txt/;
	print("My unique file name is $uniquefile\n");
	my $one = 0;
	my $onecount = 0;
	my $twoten = 0;
	my $twotencount = 0;
#	my $oneten;
#	my $onetencount;
	my $tenhundred = 0;
	my $tenhundredcount = 0; 
	my $hundredthousand = 0;
	my $hundredthousandcount = 0;
	my $overthousand = 0;
	my $overthousandcount = 0;
	my $overtwothousand = 0;
	my $overtwothousandcount = 0;
	my $overthreethousand = 0;
	my $overthreethousandcount = 0;
	open( INPUT, "$inputfile" );
	my $keycount;
	my $keycount2;
	my $totalcount;
	my $totalcount2;
        my %truecounthash;
	while ( my $line = <INPUT> ) {
		$line =~ s/\n$//;
		my @currentline = split( /\t/, $line );
		if ( $#currentline > 2 ) {
			$keycount2++;
			#print ("$currentline[0], $currentline[7], $currentline[1]\n");
			my $realkey = join("\t", $currentline[0], $currentline[1], $currentline[2]);
			#print("$realkey\n");
                        $truecounthash{$realkey}++;
			$uniquehash{ $realkey } += $currentline[3];
                        my $length = length($currentline[1]);
			$counthash{ $realkey } += $length;
			$totalcount2 += $currentline[3];
		}
	}
	close(INPUT);
	for my $key (keys %uniquehash)
	{
		$keycount += $uniquehash{$key};
		$totalcount += $counthash{$key};
	}
        my $averagecount = $totalcount / $keycount;
	print "My keycounts are $keycount and $keycount2\n";
	print "My totalcounts are $totalcount and $totalcount2\n";
	my $outputfile_name = "$dir/Length_Counts/$uniquefile";
	print ("My outputfilename is $outputfile_name\n");
	open (OUTPUT,">$outputfile_name");
	print OUTPUT "Count\tNumber_Unique\tTotal_Count_Unique\tPercent_Number_Unique\tPercent_Total_Count\n";
	for my $key (keys %uniquehash)
	{
		my $toprint = 100 * $uniquehash{$key}  / $keycount;
		my $toprinttwo = 100 * $counthash{ $key } / $totalcount;
		print OUTPUT "$key\t$uniquehash{$key}\t$counthash{$key}\t$toprint\t$toprinttwo\n";
		if ( $uniquehash{$key} < 2 )
		{
			$one += $truecounthash{$key};
			$onecount += $counthash{$key};
			next;
		}
		
		elsif ($uniquehash{$key} >= 2 && 10 >= $uniquehash{$key})
		{
			$twoten += $truecounthash{$key};
			$twotencount += $counthash{$key};
			next;
		}
		
		elsif (100 >= $uniquehash{$key}&& $uniquehash{$key} > 10)
		{
			$tenhundred += $truecounthash{$key};
			$tenhundredcount += $counthash{$key};
			next;
		}
		
		elsif (1000 >= $uniquehash{$key} && $uniquehash{$key}> 100)
		{
			$hundredthousand += $truecounthash{$key};
			$hundredthousandcount += $counthash{$key};
			next;
		}
		elsif (2000>=$uniquehash{$key} && $uniquehash{$key}> 1000)
		{
			$overthousand += $truecounthash{$key};
			$overthousandcount += $counthash{$key};
			next;
		}
		elsif (3000>=$uniquehash{$key}&& $uniquehash{$key} > 2000)
		{
			$overtwothousand += $truecounthash{$key};
			$overtwothousandcount += $counthash{$key};
			next;
		}
		elsif ($uniquehash{$key} > 3000)
		{
			$overthreethousand += $truecounthash{$key};
			$overthreethousandcount += $counthash{$key};
			next;
		}
	}
	close (OUTPUT);
	
	open (OUTPUT, ">>$permanent_output");
	#print OUTPUT "File_Name\t" . '<10' . "\t" . '<10count' . "\t" . '10<x<100' . "\t" . '10<x<100count' . "\t" . '100<x<1000' ."\t" . '100<x<1000count' ."\t" . '1000<x' . "\t" . '1000<xcount' . "\n";
	print OUTPUT "$inputfile\t$one\t$onecount\t$twoten\t$twotencount\t$tenhundred\t$tenhundredcount\t$hundredthousand\t$hundredthousandcount\t$overthousand\t$overthousandcount\t$overtwothousand\t$overtwothousandcount\t$overthreethousand\t$overthreethousandcount\t$keycount\t$totalcount\n";
	close(OUTPUT);
}
