use warnings;
use strict;
use Cwd;

my $dir = cwd;
my $outputdir = "$dir/Fish";
my @Mfilelist = qw(M1_4_3e8CAA_UNIQUE_OUTPUT.txt M1_4_8CAA_UNIQUE_OUTPUT.txt M1_CCAA_UNIQUE_OUTPUT.txt M1CAA_UNIQUE_OUTPUT.txt);
my @Yfilelist = qw(Y1_4_3e8CAA_UNIQUE_OUTPUT.txt Y1_4_8CAA_UNIQUE_OUTPUT.txt Y1_CCAA_UNIQUE_OUTPUT.txt Y1_4CAA_UNIQUE_OUTPUT.txt);
my @Ofilelist = qw(O1_4_3e8CAA_UNIQUE_OUTPUT.txt O1_4_8CAA_UNIQUE_OUTPUT.txt O1_CCAA_UNIQUE_OUTPUT.txt O1CAA_UNIQUE_OUTPUT.txt);

my $file;
my @cd8filelist;
opendir(DIR, $dir);
while(defined($file = readdir(DIR)))
{
	next if $file =~ /^\.\.?$/;
	if ($file =~ /^CD8.*AA_UNIQUE_OUTPUT.txt$/)
	{
		push(@cd8filelist, $file);
		print "pushing $file to cd8filelist\n";
	}
}
closedir(DIR);

for my $i (0..$#Mfilelist)
{
	main($Mfilelist[$i]);
}

for my $i (0..$#Yfilelist)
{
	main($Yfilelist[$i]);
}


for my $i (0..$#Ofilelist)
{
	main($Ofilelist[$i]);
}

for my $i (0..$#cd8filelist)
{
	main($cd8filelist[$i]);
}


sub main
{
	my $inputfile = shift;
	my $outputfile = $inputfile;
	$outputfile =~ s/AA_UNIQUE_OUTPUT/Fish_Input/;
	$outputfile = "$outputdir/$outputfile";
	open(INPUT, "$inputfile");
	open (OUTPUT, ">$outputfile");
	while (my $line = <INPUT>)
	{
		$line =~ s/\n$//;
		my @currentline = split(/\s+/, $line);
		my $realkey = join( ',', $currentline[0], $currentline[1], $currentline[2] );
		print OUTPUT "$realkey\t$currentline[3]\n";
	}
	close(INPUT);
	close(OUTPUT);
}