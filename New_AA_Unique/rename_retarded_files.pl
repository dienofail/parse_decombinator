use strict;
use warnings;
use Cwd;
my $dir          = getcwd;



opendir( DIR, $dir ) or die "can't opendir $dir: $!";
my $file;
my @filelist;
while ( defined( $file = readdir(DIR) ) ) {
	next if $file =~ /^\.\.?$/;                # skip . and ..
	next if $file =~ m/Decombinator_output/;
	next if $file =~ /tags/;
	if ( $file =~ m/PostSAP_AAoutput.txt.txt/ ) {
		my $newfilename = $file;
		$newfilename =~ s/.txt.txt/.txt/;
		rename $file, $newfilename;
	}
}
closedir(DIR);

