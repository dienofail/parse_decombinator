#!/usr/bin/perl -w
use strict;
use warnings;
my $AAseq = 'ASSPTGLGYNEQF';
my $file = 'L7ACPOST_SAP_COMBINE.txt';
my $outputfile3 = '33_V_DNA_output.txt';
my $Vtaginput = 'tags_trbv.txt';
my $Jtaginput = 'new_tags_trbj.txt';
my $outputfile = '33_V_DNA.fasta';
my $outputfile2 = '33_V_DNA_decombinator.fastq';
open (INPUT, ">$outputfile");
close(INPUT);
open (INPUT, ">$outputfile2");
close(INPUT);
my $Vcounter = 0;
my %Vhash;
my %Jhash;
open(INPUT, $Vtaginput) or die $!; 
while ( my $line = <INPUT> ) {
		print "Current Vcounter is $Vcounter\n";
		#$Vcounter++;
		$line =~ s/\n$//;
		my @currentline = split(/\s/, $line);
		my $tomatch = $currentline[2];
		$tomatch =~ m/\S+\|(\S+)\|\S+/;
		my $name = $1;
                $Vhash{$Vcounter} = $name;
		print "$Vcounter\t$name" . "\n";
		$Vcounter++;
}
close(INPUT);

my $Jcounter = 0;
open(INPUT, $Jtaginput) or die $!; 
while ( my $line = <INPUT> ) {
		#$Jcounter++;
		$line =~ s/\n$//;
		my @currentline = split(/\s/, $line);
		my $tomatch = $currentline[2];
				$tomatch =~ m/\S+\|(\S+)\|\S+/;
		my $name = $1;
		$Jhash{$Jcounter} = $name;
		print "$Jcounter\t$name" . "\n";
		$Jcounter++;
}
close(INPUT);
my $linecounter = 0;
open(INPUT, $file) or die;
open(OUTPUT, ">$outputfile") or die;
open (OUTPUT2, ">$outputfile2") or die;
open (OUTPUT3, ">$outputfile3") or die;
my $bool = 1;
my %alreadychecked;
my %cdr3hash;
while (my $line = <INPUT>) {
    $line =~ s/\n$//;
    my @currentline = split(/\s+/,$line);
    my $countindex = $#currentline - 1;
    if ($currentline[$#currentline] eq $AAseq)
    {
    	$linecounter++;
    	print OUTPUT3 "$currentline[0]\t$currentline[1]\t$currentline[2]\t$currentline[3]\t$currentline[4]\t$currentline[5]\n";
    	my $realkey = "$currentline[0]\t$currentline[1]\t$currentline[2]\t$currentline[3]\t$currentline[4]";
    	$cdr3hash{$realkey} = 1;
    }
    if ($currentline[$#currentline] eq $AAseq && !(defined $alreadychecked{$currentline[0]}))
    {
        print OUTPUT '>' . "$Vhash{$currentline[0]}" . '|' . "$Jhash{$currentline[1]}" . '|' . "$currentline[$countindex]\n";
        print OUTPUT "$currentline[5]\n";
        $alreadychecked{$currentline[0]} = 1;
        print OUTPUT2 '@' . "$Vhash{$currentline[0]}" . '|' . "$Jhash{$currentline[1]}" . '|' . "$currentline[$countindex]\n";
        print OUTPUT2 "$currentline[5]\n";
		print OUTPUT2 '+' . "\n";
		for my $i (1..78)
		{
			print OUTPUT2 'H';
		}
    }
    
}
close(OUTPUT);
close(OUTPUT2);
close(OUTPUT3);
close(INPUT);

my @keys = keys %cdr3hash;
print "$#keys is the number of unique CDR3s and $linecounter is the total number of unique DNA sequences\n";
