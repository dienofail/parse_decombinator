#Welcome to between_sample_overlaps.pl, a script to check the overlapping sequences between AAs. 
#Given an input directory of unique V/CDR3/J AAs with counts, this script will print out the number of overlaps
use warnings;
use strict;
use Cwd;
my %answerhash;

my $dir = "";
my $outputfile = "Between_sample_overlaps_output.txt";


opendir( DIR, $dir ) or die "can't opendir $dir: $!";
my $file;
my @filelist;
while ( defined( $file = readdir(DIR) ) ) {
	next if $file =~ /^\.\.?$/;                # skip . and ..
	next if $file =~ m/Decombinator_output/;
	next if $file =~ /tags/;
	next if $file =~ /find_unique_summary/;
	if ( $file =~ m/.txt/ ) {
		push( @filelist, $file );              #read in all files from directory
		#print("$file\n");
	}
}
closedir(DIR);

sub main 
{
	my $inputfile = shift;
	

}
