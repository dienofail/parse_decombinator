use strict;
use warnings;
use Cwd;
my $dir = getcwd;
my %permanenthash;
my %answerhash;

$dir = "$dir/OUTPUT";
my $permanent_output = "find_AAunique_table.txt";

opendir( DIR, $dir ) or die "can't opendir $dir: $!";
my $file;
my @filelist;
while ( defined( $file = readdir(DIR) ) ) {
	next if $file =~ /^\.\.?$/;                # skip . and ..
	next if $file =~ m/Decombinator_output/;
	next if $file =~ /tags/;
	if ( $file =~ m/.txt/ ) {
		push( @filelist, $file );              #read in all files from directory
		print("$file\n");
	}
}
closedir(DIR);
my $counter = 0;
for my $i ( 0 .. $#filelist ) {
	main( $filelist[$i], $i);
}

sub main
{
	my $inputfile = shift;
	my $counter = shift;
	my $now = time;
	print("Output, currently on $inputfile\n");
	my $totalcount = 0;
	open( INPUT, "$dir/$inputfile" ) or die $!;
	while ( my $line = <INPUT> ) {
		$line =~ s/\n$//;
		$totalcount++;
		my @currentline = split( /\t/, $line );
		if ( $#currentline > 2 ) {
			#print ("$currentline[0], $currentline[7], $currentline[1]\n");
			my $realkey = join("\t",$currentline[0], $currentline[1], $currentline[2]);
			$answerhash{$realkey}->[$counter] = $currentline[3];
		}
	}
	close(INPUT);
	
	for my $key (keys %answerhash)
	{
		for my $i (0..$counter)
		{
			if (defined $answerhash{$key}->[$i])
			{
				
			}
			
			else 
			{
				$answerhash{$key}->[$i] = 0;
			}
			
		}
	}
	
}

open (OUTPUT, ">$permanent_output");
print OUTPUT "V_Identifier\tCDR3\tJ_Identifier";
for my $i (0..$#filelist)
{
	print OUTPUT "\t$filelist[$i]";
}
print OUTPUT "\n";

for my $key (keys %answerhash)
{
	print OUTPUT "$key\t";
	for my $i (0..$#{$answerhash{$key}})
	{
		print OUTPUT "$answerhash{$key}->[$i]\t"
	}
	print OUTPUT"\n";
}
close(OUTPUT);
