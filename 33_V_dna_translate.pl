use warnings;
use strict;
my $Vtaginput = 'tags_trbv.txt';
my $Jtaginput = 'new_tags_trbj.txt';
my $file = '33_V_DNA_decombinator_output.txt';
my $output = '33_V_translated.txt';
my $Vcounter = 0;
my %Vhash;
my %Jhash;
open(INPUT, $Vtaginput) or die $!; 
while ( my $line = <INPUT> ) {
		print "Current Vcounter is $Vcounter\n";
		#$Vcounter++;
		$line =~ s/\n$//;
		my @currentline = split(/\s/, $line);
		my $tomatch = $currentline[2];
		$tomatch =~ m/\S+\|(\S+)\|\S+/;
		my $name = $1;
                $Vhash{$Vcounter} = $name;
		print "$Vcounter\t$name" . "\n";
		$Vcounter++;
}
close(INPUT);

my $Jcounter = 0;
open(INPUT, $Jtaginput) or die $!; 
while ( my $line = <INPUT> ) {
		#$Jcounter++;
		$line =~ s/\n$//;
		my @currentline = split(/\s/, $line);
		my $tomatch = $currentline[2];
				$tomatch =~ m/\S+\|(\S+)\|\S+/;
		my $name = $1;
		$Jhash{$Jcounter} = $name;
		print "$Jcounter\t$name" . "\n";
		$Jcounter++;
}
close(INPUT);

open(INPUT, $file);
open(OUTPUT, ">$output");
while ( my $line = <INPUT> ) {
		#$Jcounter++;
		$line =~ s/\n$//;
		my @currentline = split(/\s/, $line);
		print OUTPUT "$Vhash{$currentline[0]}\t$Jhash{$currentline[1]}\t$currentline[3]\t$currentline[4]\t$currentline[$#currentline]\n";
}
close(INPUT);
close(OUTPUT);