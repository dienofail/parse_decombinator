#!/usr/bin/env perl
use warnings;
use strict;
use Cwd;
use autodie;
use Tie::File;
my $dir = cwd;
my %uniquehash = (
10	=> 'CAT',
16	=> 'CAT',
37	=> 'CTS',
37	=> 'CVS',
2	=> 'CAI',
15	=> 'CSA',
20	=> 'CSV',
22	=> 'CAW',
);
my $tocheckone = 'CAS';
my $tochecktwo = 'CAR';
my $output = 'DUBIOUS_ALL_DOCS.txt';
opendir( DIR, $dir ) or die "can't opendir $dir: $!";
my $file;
my @filelist;
while ( defined( $file = readdir(DIR) ) ) {
	next if $file =~ /^\.\.?$/;                # skip . and ..
	next if $file =~ m/Decombinator_output/;
	next if $file =~ /tags/;
	if ( $file =~ m/POST_SAP_COMBINE.txt/ ) {
		push( @filelist, $file );              #read in all files from directory
		print("$file\n");
	}
}
closedir(DIR);
for my $i ( 0 .. $#filelist ) {
	main( $filelist[$i] );
}
sub main
{
	my $inputfile = shift;
	
#	tie my @filearray, 'Tie::File', $inputfile or die;
#	for my $i (0..$#filearray)
#	{
#		print "$filearray[$i]\n";
#		my $line = $filearray[$i];
#		my @currentline = split (/\t/,$line);
#		if (length($currentline[1]) > 20)
#		{
#			my $tocheck = $currentline[1];
#			$tocheck = substr($tocheck,2);
#			#print "$tocheck\n";
#			for my $key (keys %uniquehash)
#			{
#				if ($currentline[0] == $key)
#				{
#					$tocheck =~ m/$uniquehash{$key}/;
#					my $MAGIC_INDEX = @-;
#				}
#			}
#		} 
#	}
#	
#	untie @filearray;
	my @output;
	open(INPUT, "$inputfile");
	while (my $line = <INPUT>)
	{
		$inputfile =~ s/POST_SAP_COMBINE.txt//;
		$line =~ s/\n$//;
		chomp($line);
		my @currentline = split(/\s+/, $line);
		if (length($currentline[$#currentline]) > 20)
		{
			push(@currentline, $inputfile);
			push(@output, \@currentline);
		}
		
	}
	
	close(INPUT);
	open(OUTPUT, ">>$output");
	for my $i (0..$#output)
	{
		my $tooutput = join("\t", @{$output[$i]});
		print OUTPUT "$tooutput\n";
	}
	
	close(OUTPUT);
	
}