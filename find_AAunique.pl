use strict;
use warnings;
use Cwd;
my $dir = getcwd;
my $input = 'human_TRBV_region.fasta';
my $input2 = 'human_TRBJ_region.fasta';
my $fastarefVtag = getfastaHashRef($input);
my %Vfasta = %$fastarefVtag;
my $fastarefJtag = getfastaHashRef($input2);
my %Jfasta = %$fastarefJtag;
my %newVfasta;
my %newJfasta;

for my $key (keys %Vfasta)
{
	print ("Current key is $key\n");
	my $currentname = $key;
	my @currentname = split (/\|/, $currentname);
	my $newkey = $currentname[1];
	print ("My new key for Vfasta is $newkey\n");
	$newVfasta{$newkey} = $Vfasta{$key};
}

for my $key (keys %Jfasta)
{
	print ("Current key is $key\n");
	my $currentname = $key;
	my @currentname = split (/\|/, $currentname);
	my $newkey = $currentname[1];
	print ("My new key for Jfasta is $newkey\n");
	$newJfasta{$newkey} = $Jfasta{$key};
}


my $permanent_output = "find_AAunique_summary.txt";
print(
"Welcome to find_AAunique.pl, a tool for finding the unique DNA sequences in my Decombinator output\n"
);
print("This program is currently operating in the this work directory: $dir\n");

opendir( DIR, $dir ) or die "can't opendir $dir: $!";
my $file;
my @filelist;
while ( defined( $file = readdir(DIR) ) ) {
	next if $file =~ /^\.\.?$/;                # skip . and ..
	next if $file =~ m/Decombinator_output/;
	next if $file =~ /tags/;
	if ( $file =~ m/POST_SAP_COMBINE.txt/ ) {
		push( @filelist, $file );              #read in all files from directory
		print("$file\n");
	}
}
closedir(DIR);

for my $i ( 0 .. $#filelist ) {
	main( $filelist[$i] );
}

sub main {
	my $now = time;
	my $inputfile = shift;
	print("Output, currently on $inputfile\n");
	my $uniquefile = $inputfile;
	$uniquefile =~ s/POST_SAP_COMBINE.txt/AA_UNIQUE_OUTPUT.txt/;
	print ("My unique file name is $uniquefile\n");
	my %uniquehash;
	my %uniquecount;
	my $totalcount = 0;
	my $uniquecount = 0;
	open( INPUT, "$dir/$inputfile" ) or die $!;
	while ( my $line = <INPUT> ) {
		$line =~ s/\n$//;
		$totalcount++;
		my @currentline = split( /\s+/, $line );
		#print "$currentline[9] $currentline[10]\n";
		if ( $#currentline > 2 ) {
			#print ("$currentline[0], $currentline[7], $currentline[1]\n");
			my $realkey = join("\t",$currentline[0], $currentline[10], $currentline[1]);
			#print("$realkey\n");
			$uniquehash{ $realkey } = $realkey;
			$uniquecount { $realkey } += $currentline[9];
		}
	}
	close(INPUT);


	my $numkeys = scalar keys %uniquehash;
	my $output = "$dir/OUTPUT/$uniquefile";
	print("My output file location is $output\n");
	open( OUTPUT, ">$output" );
	for my $key ( keys %uniquehash ) {
		my $toprint = $uniquecount{$key} / $numkeys;
		my $toprint2 = $uniquecount{$key} / $totalcount;
		print OUTPUT "$uniquehash{$key}\t$uniquecount{$key}\t$toprint\t$toprint2\n";
		$uniquecount++;
	}
	close(OUTPUT);


	open (OUTPUT, ">>$permanent_output");
	{
		my $ratio = $uniquecount / $totalcount; 
		print OUTPUT "$inputfile\t$totalcount\t$uniquecount\t$ratio\n";
	}
	close (OUTPUT);
	
		$now = time - $now;
	printf(
		"\n\nTotal running time: %02d:%02d:%02d\n\n",
		int( $now / 3600 ),
		int( ( $now % 3600 ) / 60 ),
		int( $now % 60 )
	);
}
#
#
sub getfastaHashRef {
	my ($filename) = @_;
	chomp($filename);

	my $trying = "Unable to open $filename.\n";

	# read in the file with the given filename
	open( INPUTFILE, $filename ) or die($trying);

	my @filedata = <INPUTFILE>;

	close INPUTFILE;

	my %seqs;
	my $seqname;
	my $seq = '';

	# read in each line and check against regular expression
	foreach my $line (@filedata) {
		if    ( $line =~ /^\s*$/ ) { next; }    # Ignore blank lines
		elsif ( $line =~ /^\s*#/ ) { next; }    # Ignore comments
		elsif ( $line =~ /^\>\s*(.*)\s*$/
		  )    # The name of the sequence starts with ">" in FASTA
		{
			my $temp = $1
			  ; #Only what matches within the brackets (.*) in the above regular expression
			$temp =~ s/\s//sg;
			if ( length($seq) > 0 ) {
				$seq =~ s/\s//sg;
				$seqs{$seqname} = $seq;
				$seq = '';
			}
			$seqname = $temp;
			next;
		}
		else { $seq .= $line; }
	}
	$seq =~ s/\s//sg;
	$seqs{$seqname} = $seq;

	return \%seqs;
}

