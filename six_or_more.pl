use warnings;
use strict;
use Cwd;
my $six_or_more_file = 'six_or_more_summary.txt';
my $dir              = cwd;
my $currentdir       = $dir;
print
"Welcome to six_or_more.pl, a script designed to match V_CDR3s that have six or more Vs to the DNA seqs of the files";

my @filelist;
my $file;
opendir( DIR, $currentdir ) or die "can't opendir $dir: $!";
while ( defined( $file = readdir(DIR) ) ) {
	next if $file =~ /^\.\.?$/;                   # skip . and ..
	next if $file =~ m/Decombinator_output/;
	next if $file =~ /tags/;
	next if $file =~ /find_unique_summary/;
	next if $file =~ /Summary/;
	next if $file =~ /six_or_more_summary.txt/;
	if ( $file =~ m/.txt/ ) {
		push( @filelist, $file );    #read in all files from directory
		                             #print("$file\n");
	}
}
closedir(DIR);
my @list_cdr3s;

open( INPUT, "$six_or_more_file" );
while ( my $line = <INPUT> ) {
	$line =~ s/\n$//;
	my @currentline = split( /\t/, $line );
	my @topush;
	push( @topush, $currentline[0] );
	my $topushname = $currentline[2];
	$topushname =~ s/AA_OVERLAP.txt/POST_SAP_COMBINE.txt/;
	push( @topush,     $topushname );
	push( @list_cdr3s, \@topush );
}
close(INPUT);

for my $i ( 0 .. $#filelist ) {
	main( $filelist[$i] );
}

sub main {
	my $inputfile = shift;
	print("Reading current $inputfile\n");
	my $output = $inputfile;
	$output =~ s/POST_SAP_COMBINE.txt/.fasta/;
	$output = "$currentdir/six_or_more_fasta/$output";
	my $revcompoutput = $output;
	$revcompoutput =~ s/\.fasta/rev_comp.fasta/;
	print "My $output and $revcompoutput\n";
	my @six_array;
	my @toprintseqs;

	for my $i ( 0 .. $#list_cdr3s ) {
		if ( $list_cdr3s[$i]->[1] eq $inputfile ) {
			push( @six_array, $list_cdr3s[$i]->[0] );
#			print(
#"I have matching files and I'm pushing $list_cdr3s[$i]->[0] into six_array\n"
#			);
		}
	}
	open( INPUT, "$inputfile" );
	while ( my $line = <INPUT> ) {
		$line =~ s/\n$//;
		my @currentline = split( /\s+/, $line );
		for my $i ( 0 .. $#six_array ) {
			if ( $six_array[$i] eq $currentline[$#currentline] ) {
				push( @toprintseqs, $currentline[5] );
				#print "Pushing $currentline[5] into my array on $i\n";
			}
		}

	}
	close(INPUT);
	my $toprintcounter = 0;
	open( OUTPUT,  ">$output" );
	open( OUTPUT2, ">$revcompoutput" );
	for my $i ( 0 .. $#toprintseqs ) {
		print "printing to printseqs\n";
		my $fastaname = '>' . "$inputfile" . "$toprintcounter";
		print OUTPUT "$fastaname\n";
		print OUTPUT "$toprintseqs[$i]\n";
		my $revcomp = revdnacomp( $toprintseqs[$i] );
		print OUTPUT2 "$revcomp\n";
	}
	close(OUTPUT);
	close(OUTPUT2);

}

sub revdnacomp {
	my $dna     = shift;
	my $revcomp = reverse($dna);

	$revcomp =~ tr/ACGTacgt/TGCAtgca/;

	return $revcomp;
}
