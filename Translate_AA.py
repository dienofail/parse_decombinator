total_mismatches = 0
def get_translated_sequences(handle, handle_results, chain, with_outframe=False, fullaaseq=False):

    # # TRANSLATES CLASSIFIERS TO AA SEQUENCES VIA THEIR NT SEQUENCE
    # # Default settings are -
    # # chain = "beta" or chain = "alpha"
    # # with_outframe=True or False: writes all aa seqeunces to file, including those that are out-of-frame (with stop codon symbol *)
    # # fullaaseq=True or False: True writes the whole V(D)J aa sequence to file, False, writes only the CDR3 region.

    from Bio.Seq import Seq
    from Bio import SeqIO
    from Bio.Alphabet import generic_dna
    import string
    import re

    handle_vb = open("human_TRBV_region.fasta", "rU")
    handle_jb = open("human_TRBJ_region.fasta", "rU")
    handle_va = open("human_TRAV_region.fasta", "rU")
    handle_ja = open("human_TRAJ_region.fasta", "rU")
    handle_vg = open("human_TRGV_region.fasta", "rU")
    handle_jg = open("human_TRGJ_region.fasta", "rU")
    handle_vd = open("human_TRDV_region.fasta", "rU")
    handle_jd = open("human_TRDJ_region.fasta", "rU")
    
    vb_raw = list(SeqIO.parse(handle_vb, "fasta"))
    handle_vb.close()
    jb_raw = list(SeqIO.parse(handle_jb, "fasta"))
    handle_jb.close()
    va_raw = list(SeqIO.parse(handle_va, "fasta"))
    handle_va.close()
    ja_raw = list(SeqIO.parse(handle_ja, "fasta"))
    handle_ja.close()
    vg_raw = list(SeqIO.parse(handle_vg, "fasta"))
    handle_vg.close()
    jg_raw = list(SeqIO.parse(handle_jg, "fasta"))
    handle_jg.close()
    vd_raw = list(SeqIO.parse(handle_vd, "fasta"))
    handle_vd.close()
    jd_raw = list(SeqIO.parse(handle_jd, "fasta"))
    handle_jd.close()

    vb_regions = []
    for i in range(0, len(vb_raw)):
        vb_regions.append(string.upper(vb_raw[i].seq))

    jb_regions = []
    for i in range(0, len(jb_raw)):
        jb_regions.append(string.upper(jb_raw[i].seq))

    va_regions = []
    for i in range(0, len(va_raw)):
        va_regions.append(string.upper(va_raw[i].seq))

    ja_regions = []
    for i in range(0, len(ja_raw)):
        ja_regions.append(string.upper(ja_raw[i].seq))

    vg_regions = []
    for i in range(0, len(vg_raw)):
        vg_regions.append(string.upper(vg_raw[i].seq))

    jg_regions = []
    for i in range(0, len(jg_raw)):
        jg_regions.append(string.upper(jg_raw[i].seq))

    vd_regions = []
    for i in range(0, len(vd_raw)):
        vd_regions.append(string.upper(vd_raw[i].seq))

    jd_regions = []
    for i in range(0, len(jd_raw)):
        jd_regions.append(string.upper(jd_raw[i].seq))

    write_to = open(str(handle_results) + '.txt', "w")
    total_current_seqs = 0
    in_scope_errors = 0
    match_index = 0
    if chain == "beta":
        global total_mismatches
        for line in handle:
            elements = line.rstrip("\n")
            #classifier = re.split("\s+",elements)
            classifier = elements.split()
            last_classifier_index = len(classifier) - 1
            #print classifier[last_classifier_index] + "\n"
            total_current_seqs += 1
            #print len(classifier)
            #print line
            #print str(classifier[0])
            #print classifier[1]
            
            if len(classifier) > 1:
                v = int(classifier[0])
                j = int(classifier[1])
                delv = int(classifier[2])
                delj = int(classifier[3])
                ins = str(classifier[4].replace(' ', ''))
            # elif len(classifier) == 9:
                # v = int(classifier[0])
                # j = int(classifier[1])
                # delv = int(classifier[2])
                # delj = int(classifier[3])
                # ins = ''

            if delv != 0:
                used_v = vb_regions[v][:-delv]
            elif delv == 0:
                used_v = vb_regions[v]

            if delj != 0:
                used_j = jb_regions[j][delj:]
            elif delj == 0:
                used_j = jb_regions[j]

            seq = str(used_v + ins + used_j)
            start = len(seq) % 3 + 2
            aaseq = Seq(str(seq[start:]), generic_dna).translate()

            if fullaaseq == True:
                if with_outframe == True:
                    print >> write_to, elements + "\t" + str(aaseq)
                elif '*' not in aaseq:
                    print >> write_to, elements + "\t" + str(aaseq)
            else:     
                if re.findall('FG.G', str(aaseq)) and re.findall('C', str(aaseq)):
                    indices = [i for i, x in enumerate(aaseq) if x == 'C']
                    upper = str(aaseq).find(re.findall('FG.G', str(aaseq))[0])
                    for i in indices:
                        if i < upper:
                            lower = i
                    cdr3 = aaseq[lower:upper]
                    cdr3 = cdr3[1:]
                    cdr3 = str(cdr3)
                    cdr3.strip()
                    classifier[last_classifier_index].strip()
                    #print "Checking" + classifier[last_classifier_index] + cdr3
                    #print cdr3 == classifier[last_classifier_index]

                    if (classifier[last_classifier_index] == cdr3):
                        match_index += 1
                    else:
                        total_mismatches += 1
                        in_scope_errors += 1
                        #print 'I have a mismatch at' + classifier[last_classifier_index] + "\t" + cdr3 + "\n"
                    if with_outframe == True:
                        print >> write_to, elements + "\t" + cdr3
                    elif '*' not in aaseq:
                        print >> write_to, elements + "\t" + cdr3
        print str(total_mismatches) + "\t" + str(in_scope_errors) + "\t" + str(total_current_seqs) + "\t" + str(match_index) + "\n"

            
    handle.close()
    write_to.close()



print 'Welcome to AA translate, a tool to translate the Decombinator parses into the desired AA output'

import os
import re
import platform
filestoanalyze = []
resultslist = []
for files in os.listdir("."):
    if files.endswith("POST_SAP_COMBINE.txt"):
        filestoanalyze.append(files)
for myfiles in filestoanalyze:
    toappend = re.sub('POST_SAP_COMBINE.txt', '_PostSAP_AAoutput.txt', myfiles)
    resultslist.append(toappend)
print str(filestoanalyze)
print str(resultslist)
currentpath = os.getcwd()

if platform.system() == 'Windows':
    newpath = currentpath + '\\New_AA_Unique' + '\\'  # # Ensure correct for specified platform
    if not os.path.exists(newpath):
        os.makedirs(newpath)
        
for idx, val in enumerate(filestoanalyze):
    print idx, val
    name_results = resultslist[idx]
    print ("Currently using this name on name_results:\n")
    print name_results
    get_translated_sequences(open(val, "rU"), handle_results=newpath + str(name_results), chain="beta", with_outframe=False, fullaaseq=False)